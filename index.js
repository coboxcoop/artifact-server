const Koa = require('koa')
const KoaRouter = require('koa-router')
const BodyParser = require('koa-bodyparser')
const path = require('path')
const ArtifactStore = require('artifact-store')
const cors = require('@koa/cors')
const mime = require('mime')
const log = require('debug')('artifact-server')
const homeDir = require('os').homedir()

const app = new Koa()
const router = new KoaRouter()

module.exports = async function (options) {
  const host = options.host || 'localhost'
  const port = options.port || 1234

  console.log(options.pataka ? 'Pataka' : 'Artifact-store')

  const storage = options.storage || path.join(homeDir, '.artifact-store')
  console.log(`Using local storage ${storage}`)

  const store = new ArtifactStore(storage, options)
  await store.ready()
  if (!options.pataka) console.log(' Your drive address: ', store.driveAddress.toString('hex'))

  app.use(BodyParser())
  app.use(async (ctx, next) => {
    log(`${ctx.method} ${ctx.url} ${JSON.stringify(ctx.request.body)}`)
    await next()
  })
  app.use(cors())
  app.use(router.routes())
  // .use(router.allowedMethods())

  router.post('/drive', async (ctx) => {
    const ok = await store.addDrive(ctx.request.body.driveAddress)
      .catch((err) => {
        console.log(err)
        return { error: err.message }
      })
    ctx.status = ok.error ? 422 : 200
    ctx.body = ok.error ? ok : { ok: true }
  })

  async function getStream (ctx) {
    const fileToGet = ctx.params.fileToGet
    const driveAddress = Buffer.from(ctx.params.driveAddress, 'hex')
    const encryptionKey = Buffer.from(ctx.params.encryptionKey, 'hex')
    const options = {
      start: parseInt(ctx.params.start),
      end: parseInt(ctx.params.end)
    }
    const src = await store.getReadStream(fileToGet, driveAddress, encryptionKey, options)
    ctx.type = mime.getType(path.extname(fileToGet))
    ctx.body = src
    ctx.status = 200
  }

  if (!options.pataka) {
    router.post('/file', async (ctx) => {
      const output = await store.addFile(ctx.request.body.localFileName, {
        fileEncryptionKey: ctx.request.body.fileEncryptionKey,
        targetFileName: ctx.request.body.targetFileName
      })
        .catch((err) => {
          console.log(err)
          return { error: err.message }
        })
      // look up error-handling conventions in koa
      ctx.status = output.error ? 422 : 200
      if (!output.error) {
        output.link = `http://${host}:${port}/file/${output.fileName}/${output.driveAddress}/${output.fileEncryptionKey}`
      }
      ctx.body = output
    })

    router.get('/file/:fileToGet/:driveAddress/:encryptionKey', getStream)
    router.get('/file/:fileToGet/:driveAddress/:encryptionKey/:start', getStream)
    router.get('/file/:fileToGet/:driveAddress/:encryptionKey/:start/:end', getStream)
  }

  router.post('/close', async (ctx) => {
    const ok = await store.close()
      .catch((err) => {
        console.log(err)
        return { error: err.message }
      })
    ctx.status = (ok && ok.error) ? 422 : 200
    ctx.body = (ok && ok.error) ? ok : { ok: true }
  })

  app.listen(port, host, () => {
    console.log(`Listening on http://${host}:${port}`)
  })
}
