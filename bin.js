#!/usr/bin/env node
const artifactServer = require('.')
const options = require('minimist')(process.argv.slice(2))

if (options.help) {
  console.log(`Command line options:
--help    display usage information
--host <hostname>  set hostname (defaults to localhost)
--port <port>      set port number (defaults to 1234)
--storage <path>   set local storage directory (defaults to ~/.artifact-store)
--pataka           be a pataka
  `)
  process.exit(0)
}

artifactServer(options)
