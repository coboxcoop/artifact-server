# Artifact Server

to install, npm link [artifact-store](https://gitlab.com/ahau/artifact-store) and [artifact-render](https://gitlab.com/coboxcoop/try-render-media)
to run:
`./bin.js <options>`

Options can be:

- `--help` display usage information
- `--host <hostname>` set hostname (defaults to localhost)
- `--port <port>` set port number (defaults to 1234)
- `--storage <path>` set local storage directory (defaults to ~/.artifact-store)
- `--pataka` be a pataka


### http endpoints:

#### coming soon: GET `/` - send front-end to browser (not yet merged)

#### POST `/drive` - connect to a driveAddress, and if in pataka mode, fully replicate the drive
- `driveAddress` - the address of a remote drive (32 bytes, hex) 

responds with:
```json
{ "ok": "true" }
```
or
```json
{ "error": "error message" }
```

#### POST `/file` - add a file to your drive (not available in pataka mode)
- `localFileName`: the path to a local file to add
- `fileEncryptionKey`:key to use for encrypting the file (32 bytes, hex) - optional.  If not given, will create one.
- `targetFileName`: the path to give the file on the drive - optional. If not given, the original filename will be used.

responds with:
```json
{
  "driveAddress": "...",
  "fileEncryptionKey": "...",
  "fileName": "..."
}
```

#### GET `/file/:fileToGet/:driveAddress/:encryptionKey`
#### GET `/file/:fileToGet/:driveAddress/:encryptionKey/:start`
#### GET `/file/:fileToGet/:driveAddress/:encryptionKey/:start/:end` - retrieve a file (not available in pataka mode)
- `fileToGet` - filepath
- `driveAddress` - address of drive (hex)
- `encryptionKey` - encryption key (hex)
- `start` - byte offset to begin stream (optional - defaults to  0)
- `end` - byte offset to stop stream (optional - defaults to EOF)

responds with a binary stream (for use with [`render-media`](https://github.com/feross/render-media)) - can also be viewed directly in the browser (but seeking not available)
